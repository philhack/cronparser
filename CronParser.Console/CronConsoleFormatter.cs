﻿namespace CronParser.Console
{
    public static class CronConsoleFormatter
    {
        private const int NAME_COLUMN_LENGTH = 14;

        public static string FormatName(this string name) {
            return name.PadRight(NAME_COLUMN_LENGTH, ' ');
        }
    }
}