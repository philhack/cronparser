﻿using System;
using System.Reflection.Metadata;
using CronParser.Core;
using CronParser.Core.Domain;
using CronParser.Core.Factories;
using CronParser.Core.Validators;

namespace CronParser.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            try {
                var cronInputValidator = new CronInputValidator();
                var cronTabLineFactory = new CronTabLineFactory();
                cronInputValidator.Validate(args[0]);
                var cronTab = cronTabLineFactory.Create(args[0]);

                foreach (var cronTabLine in cronTab) {
                    System.Console.WriteLine($"{cronTabLine.Name.FormatName()}{cronTabLine.OutputValue}");
                }
            }
            catch(Exception ex) {
                System.Console.WriteLine($"Error: {ex.Message}");
            }
        }
    }
    
}
