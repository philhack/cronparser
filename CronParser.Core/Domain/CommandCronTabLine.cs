﻿using CronParser.Core.Parsers;

namespace CronParser.Core.Domain
{
    public class CommandCronTabLine : CronTabLine
    {
        public CommandCronTabLine(string inputValue) : base("command", "command", inputValue, new CommandParser()) {
        }
    }
}