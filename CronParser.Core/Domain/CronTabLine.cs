﻿using CronParser.Core.Parsers;

namespace CronParser.Core.Domain
{
    public interface ICronTabLine
    {
        string Key { get; set; }
        string Name { get; }
        string InputValue { get; }
        string OutputValue { get; }
    }

    public abstract class CronTabLine : ICronTabLine
    {
        private readonly IParser _parser;

        protected CronTabLine(string key, string name, string inputValue, IParser parser) {
            Key = key;
            Name = name;
            InputValue = inputValue;
            _parser = parser;
        }

        public string Key { get; set; }
        public string Name { get; }
        public string InputValue { get; }
        public string OutputValue => _parser.Parse(InputValue);
    }
}