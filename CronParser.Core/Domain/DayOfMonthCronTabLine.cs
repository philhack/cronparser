﻿using CronParser.Core.Parsers;

namespace CronParser.Core.Domain
{
    public class DayOfMonthCronTabLine : CronTabLine
    {
        public DayOfMonthCronTabLine(string inputValue) : base("dayOfMonth", "day of month", inputValue, new DayOfMonthParser()) {
        }
    }
}