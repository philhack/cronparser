﻿using CronParser.Core.Parsers;

namespace CronParser.Core.Domain
{
    public class DayOfWeekCronTabLine : CronTabLine
    {
        public DayOfWeekCronTabLine(string inputValue) :
            base("dayOfWeek", "day of week", inputValue, new DayOfWeekParser()) {
        }
    }
}