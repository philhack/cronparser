﻿using CronParser.Core.Parsers;

namespace CronParser.Core.Domain
{
    public class HourCronTabLine : CronTabLine
    {
        public HourCronTabLine(string inputValue) : base("hour", "hour", inputValue, new HourParser()) {
        }
    }
}