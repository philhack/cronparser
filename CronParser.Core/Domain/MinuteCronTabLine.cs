﻿using CronParser.Core.Parsers;

namespace CronParser.Core.Domain
{
    public class MinuteCronTabLine : CronTabLine
    {
        public MinuteCronTabLine(string inputValue) : base("minute", "minute", inputValue, new MinuteParser()) {
        }
    }
}