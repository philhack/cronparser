﻿using CronParser.Core.Parsers;

namespace CronParser.Core.Domain
{
    public class MonthCronTabLine : CronTabLine
    {
        public MonthCronTabLine(string inputValue) : base("month", "month", inputValue, new MonthParser()) {
        }
    }
}