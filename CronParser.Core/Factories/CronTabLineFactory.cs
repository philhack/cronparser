﻿using System.Collections.Generic;
using CronParser.Core.Domain;

namespace CronParser.Core.Factories
{
    public class CronTabLineFactory
    {
        public List<ICronTabLine> Create(string input) {
            var arguments = input.Split(" ");

            var cronTabLines = new List<ICronTabLine> {
                new MinuteCronTabLine(arguments[0]),
                new HourCronTabLine(arguments[1]),
                new DayOfMonthCronTabLine(arguments[2]),
                new MonthCronTabLine(arguments[3]),
                new DayOfWeekCronTabLine(arguments[4]),
                new CommandCronTabLine(arguments[5])
            };

            return cronTabLines;
        }
    }
}