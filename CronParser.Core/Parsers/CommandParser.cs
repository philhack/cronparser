﻿using System;

namespace CronParser.Core.Parsers
{
    public class CommandParser : Parser
    {
        public override string Parse(string input) {
            if (input == null) {
                throw new ArgumentNullException(nameof(input), "Command can not be null");
            }

            if (input == string.Empty) {
                throw new ArgumentException("Command can not be an empty string", nameof(input));
            }

            return input;
        }
    }
}