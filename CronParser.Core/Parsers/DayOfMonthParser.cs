﻿using CronParser.Core.Rules;

namespace CronParser.Core.Parsers
{
    public class DayOfMonthParser : Parser
    {
        private readonly int[] _allDaysOfTheMonth = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                                                    20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};

        public override string Parse(string input) {
            var assessment = new CronRuleAssessment(input);

            var asteriskCronRule = new AsteriskCronRule(_allDaysOfTheMonth);
            var multipleCronRule = new MultipleCronRule(_allDaysOfTheMonth);
            var stepCronRule = new StepCronRule(_allDaysOfTheMonth);
            var rangeCronRule = new RangeCronRule(_allDaysOfTheMonth);
            var singleCronRule = new SingleCronRule(_allDaysOfTheMonth);

            asteriskCronRule.SetNextAssessment(multipleCronRule);
            multipleCronRule.SetNextAssessment(stepCronRule);
            multipleCronRule.SetNextAssessment(stepCronRule);
            stepCronRule.SetNextAssessment(rangeCronRule);
            rangeCronRule.SetNextAssessment(singleCronRule);

            return asteriskCronRule.Access(assessment).Value;
        }
    }
}