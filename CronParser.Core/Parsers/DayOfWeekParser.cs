﻿using CronParser.Core.Rules;

namespace CronParser.Core.Parsers
{
    public class DayOfWeekParser : Parser
    {
        private readonly int[] _allDaysOfTheWeekStartingWithMonday = {1, 2, 3, 4, 5, 6, 7};

        public override string Parse(string input) {
            var assessment = new CronRuleAssessment(input);

            var asteriskCronRule = new AsteriskCronRule(_allDaysOfTheWeekStartingWithMonday);
            var multipleCronRule = new MultipleCronRule(_allDaysOfTheWeekStartingWithMonday);
            var stepCronRule = new StepCronRule(_allDaysOfTheWeekStartingWithMonday);
            var rangeCronRule = new RangeCronRule(_allDaysOfTheWeekStartingWithMonday);
            var singleCronRule = new SingleCronRule(_allDaysOfTheWeekStartingWithMonday);

            asteriskCronRule.SetNextAssessment(multipleCronRule);
            multipleCronRule.SetNextAssessment(stepCronRule);
            stepCronRule.SetNextAssessment(rangeCronRule);
            rangeCronRule.SetNextAssessment(singleCronRule);

            return asteriskCronRule.Access(assessment).Value;
        }
    }
}