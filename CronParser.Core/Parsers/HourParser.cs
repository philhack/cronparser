﻿using CronParser.Core.Rules;

namespace CronParser.Core.Parsers
{
    public class HourParser : Parser
    {
        private readonly int[] _allHours = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23};

        public override string Parse(string input) {
            var assessment = new CronRuleAssessment(input);
            
            var asteriskCronRule = new AsteriskCronRule(_allHours);
            var multipleCronRule = new MultipleCronRule(_allHours);
            var stepCronRule = new StepCronRule(_allHours);
            var rangeCronRule = new RangeCronRule(_allHours);
            var singleCronRule = new SingleCronRule(_allHours);
            
            asteriskCronRule.SetNextAssessment(multipleCronRule);
            multipleCronRule.SetNextAssessment(stepCronRule);
            stepCronRule.SetNextAssessment(rangeCronRule);
            rangeCronRule.SetNextAssessment(singleCronRule);
            
            return asteriskCronRule.Access(assessment).Value;
        }
    }
}