﻿using CronParser.Core.Rules;

namespace CronParser.Core.Parsers
{
    public class MonthParser : Parser
    {
        private readonly int[] _allMonthsOfTheYear = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};

        public override string Parse(string input) {
            var assessment = new CronRuleAssessment(input);

            var asteriskCronRule = new AsteriskCronRule(_allMonthsOfTheYear);
            var multipleCronRule = new MultipleCronRule(_allMonthsOfTheYear);
            var stepCronRule = new StepCronRule(_allMonthsOfTheYear);
            var rangeCronRule = new RangeCronRule(_allMonthsOfTheYear);
            var singleCronRule = new SingleCronRule(_allMonthsOfTheYear);

            asteriskCronRule.SetNextAssessment(multipleCronRule);
            multipleCronRule.SetNextAssessment(stepCronRule);
            stepCronRule.SetNextAssessment(rangeCronRule);
            rangeCronRule.SetNextAssessment(singleCronRule);

            return asteriskCronRule.Access(assessment).Value;
        }
    }
}