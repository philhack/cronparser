﻿namespace CronParser.Core.Parsers
{
    public interface IParser
    {
        string Parse(string input);
    }

    public abstract class Parser : IParser
    {
        public abstract string Parse(string input);
    }
}