﻿namespace CronParser.Core.Rules
{
    public class AsteriskCronRule : CronRule
    {
        public AsteriskCronRule(int[] allPossibleItems) {
            AllPossibleItems = allPossibleItems;
        }

        protected sealed override int[] AllPossibleItems { get; set; }

        protected override bool ExecuteRule(CronRuleAssessment assessment) {
            if (assessment.Input == "*" && assessment.Input.Length == 1) {
                assessment.Result.UpdateResult(string.Join(" ", AllPossibleItems));
                return true;
            }

            return false;
        }
    }
}