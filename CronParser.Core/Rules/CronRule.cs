﻿namespace CronParser.Core.Rules
{
    public interface ICronRule
    {
        void SetNextAssessment(CronRule cronRule);
        CronRuleAssessmentResult Access(CronRuleAssessment assessment);
    }

    public abstract class CronRule : ICronRule
    {
        protected abstract int[] AllPossibleItems { get; set; }

        protected CronRule NextCronRule;
        protected abstract bool ExecuteRule(CronRuleAssessment assessment);

        public void SetNextAssessment(CronRule cronRule) {
            NextCronRule = cronRule;
        }

        public CronRuleAssessmentResult Access(CronRuleAssessment assessment) {
            var result = ExecuteRule(assessment);
            switch (result) {
                case true:
                    return assessment.Result;
                case false when NextCronRule == null:
                    return assessment.Result;
            }

            return NextCronRule.Access(assessment);
        }
    }
}