﻿namespace CronParser.Core.Rules
{
    public class CronRuleAssessment
    {
        public CronRuleAssessment(string input) {
            Input = input;
            Result = new CronRuleAssessmentResult();
        }

        public string Input { get; }
        public CronRuleAssessmentResult Result { get; }
    }
}