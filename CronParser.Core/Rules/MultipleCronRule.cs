﻿using System;
using System.Linq;

namespace CronParser.Core.Rules
{
    public class MultipleCronRule : CronRule
    {
        public MultipleCronRule(int[] allPossibleItems) {
            AllPossibleItems = allPossibleItems;
        }

        protected sealed override int[] AllPossibleItems { get; set; }

        protected override bool ExecuteRule(CronRuleAssessment assessment) {
            if (assessment.Input.Contains(","))
            {
                var minutes = assessment.Input.Split(",");

                ValidateMultipleMinutesAreWithinAcceptableRange(minutes);

                assessment.Result.UpdateResult(string.Join(" ", minutes));
                return true;
            }

            return false;
        }

        private void ValidateMultipleMinutesAreWithinAcceptableRange(string[] minutes)
        {
            if (minutes.Any(minute => !AllPossibleItems.Contains(Convert.ToInt32((string) minute))))
            {
                throw new ArgumentOutOfRangeException(
                    $"Minutes are outside of the range of acceptable minutes. They must be at least {AllPossibleItems.First()} and at most {AllPossibleItems.Last()}");
            }
        }
    }
}