﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace CronParser.Core.Rules
{
    public class RangeCronRule : CronRule
    {
        private const string RANGE_REGEX_PATTERN = @"^(?<first>[0-9]+)(-{1})(?<last>[0-9]+)$";
        
        public RangeCronRule(int[] allPossibleItems) {
            AllPossibleItems = allPossibleItems;
        }

        protected sealed override int[] AllPossibleItems { get; set; }

        protected override bool ExecuteRule(CronRuleAssessment assessment) {

            var matchRangePattern = Regex.Match(assessment.Input, RANGE_REGEX_PATTERN);
            if (matchRangePattern.Success)
            {
                var firstMinute = Convert.ToInt32(matchRangePattern.Groups["first"].Value);
                var lastMinute = Convert.ToInt32(matchRangePattern.Groups["last"].Value);

                var inclusiveMinutes = AllPossibleItems.Where(x => x >= firstMinute && x <= lastMinute);

                assessment.Result.UpdateResult(string.Join(" ", inclusiveMinutes));
                return true;
            }

            return false;
        }
    }
}