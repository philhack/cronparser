﻿using System;
using System.Linq;

namespace CronParser.Core.Rules
{
    public class SingleCronRule : CronRule
    {
        public SingleCronRule(int[] allPossibleItems) {
            AllPossibleItems = allPossibleItems;
        }

        protected sealed override int[] AllPossibleItems { get; set; }

        protected override bool ExecuteRule(CronRuleAssessment assessment) {
            if (InputWithinRange(assessment.Input))
            {
                assessment.Result.UpdateResult(assessment.Input);
                return true;
            }

            return false;
        }

        private bool InputWithinRange(string input)
        {
            var inputAsNumber = Convert.ToInt32(input);
            return inputAsNumber >= AllPossibleItems.First() && inputAsNumber <= AllPossibleItems.Last();
        }
    }
}