﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CronParser.Core.Rules
{
    public class StepCronRule : CronRule
    {
        private const string CRON_STEP_REGEX_PATTERN = @"^(?<prefix>[\*]{1}|[0-9]+[-{1}][0-9]+)[\/]{1}(?<step>[0-9]+)$";
        
        public StepCronRule(int[] allPossibleItems) {
            AllPossibleItems = allPossibleItems;
        }

        protected sealed override int[] AllPossibleItems { get; set; }

        protected override bool ExecuteRule(CronRuleAssessment assessment) {
            var matchStepPattern = Regex.Match(assessment.Input, CRON_STEP_REGEX_PATTERN);
            if (matchStepPattern.Success)
            {
                var prefix = matchStepPattern.Groups["prefix"].Value;
                var step = Convert.ToInt32(matchStepPattern.Groups["step"].Value);
                if (prefix == "*") {
                    assessment.Result.UpdateResult(string.Join(" ", AllPossibleItems.Where(minute => (minute % step) == 0)));
                    return true;
                }

                if (prefix.Contains("-"))
                {
                    var splitPrefix = prefix.Split("-");
                    var firstNumber = Convert.ToInt32(splitPrefix.First());
                    var lastNumber = Convert.ToInt32(splitPrefix.Last());
                    var numberOfIterations = (firstNumber + lastNumber) / step;
                    var total = new List<int> { firstNumber };

                    for (int i = 1; i < numberOfIterations; i++)
                    {
                        total.Add(total.Last() + step);
                    }

                    assessment.Result.UpdateResult(string.Join(" ", total));
                    return true;
                }
            }
            return false;
        }
    }
}