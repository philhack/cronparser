﻿namespace CronParser.Core.Rules
{
    public class CronRuleAssessmentResult
    {
        public CronRuleAssessmentResult() {
            Success = false;
            Value = string.Empty;
        }

        public bool Success { get; private set; }
        public string Value { get; private set; }

        public void UpdateResult(string result) {
            if (!string.IsNullOrEmpty(result)) {
                Success = true;
                Value = result;
            }
        }
    }
}