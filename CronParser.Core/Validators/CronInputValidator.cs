﻿using System;

namespace CronParser.Core.Validators
{
    public class CronInputValidator
    {
        public void Validate(string commandLineArgument) {
            GuardCondition_CanNotBeNull(commandLineArgument);
            GuardCondition_ArgumentLength(commandLineArgument);
        }

        private void GuardCondition_CanNotBeNull(string commandLineArgument) {
            if (string.IsNullOrEmpty(commandLineArgument)) {
                throw new ArgumentNullException(nameof(commandLineArgument), "Command line arguments can not be null.");
            }
        }

        private void GuardCondition_ArgumentLength(string commandLineArgument) {
            const int numberOfRequiredAguments = 6;
            string[] arguments;

            try {
                arguments = commandLineArgument.Split(" ");
            }
            catch (Exception) {
                throw new ArgumentException($"Incorrect syntax used. Ensure 6 cron expressions are passed in, each separated by a single space, as a single argument.");
            }
            
            if (arguments.Length != numberOfRequiredAguments) {
                throw new ArgumentException($"Incorrect number of CronTab arguments. {arguments.Length} were passed in. {numberOfRequiredAguments} are required.");
            }
        }
    }
}