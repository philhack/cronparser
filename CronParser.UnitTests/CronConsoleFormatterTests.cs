﻿using CronParser.Console;
using FluentAssertions;
using NUnit.Framework;

namespace CronParser.UnitTests
{
    public class CronConsoleFormatterTests
    {
        [Test]
        public void Given_a_6_character_name_when_FormatName_is_called_then_the_string_is_14_characters_long() {
            var sixCharacters = "minute";

            sixCharacters.FormatName().Length.Should().Be(14);
        }
    }
}