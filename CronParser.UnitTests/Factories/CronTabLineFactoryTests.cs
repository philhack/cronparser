﻿using System.Linq;
using CronParser.Core.Domain;
using CronParser.Core.Factories;
using FluentAssertions;
using NUnit.Framework;

namespace CronParser.UnitTests.Factories
{
    public class CronTabLineFactoryTests
    {
        private const string VALID_COMMAND_LINE_ARGUMENTS = "*/15 0 1,15 * 1-5 /usr/bin/find";
        CronTabLineFactory _cronTabLineFactory;

        [SetUp]
        public void Given_a_CronTabLineFactory() {
            
            _cronTabLineFactory = new CronTabLineFactory();
        }

        [Test]
        public void When_valid_input_is_passed_it_then_it_should_return_the_correct_minutes()
        {
            var cronTabLines = _cronTabLineFactory.Create(VALID_COMMAND_LINE_ARGUMENTS);

            var minuteCrontTabLine = cronTabLines.First(x => x.Key == "minute");
            minuteCrontTabLine.InputValue.Should().Be("*/15");
            minuteCrontTabLine.Name.Should().Be("minute");
            minuteCrontTabLine.OutputValue.Should().Be("0 15 30 45");
            minuteCrontTabLine.Should().BeOfType<MinuteCronTabLine>();
        }

        [Test]
        public void When_valid_input_is_passed_it_then_it_should_return_the_correct_hours()
        {
            var cronTabLines = _cronTabLineFactory.Create(VALID_COMMAND_LINE_ARGUMENTS);

            var hourCronTabLine = cronTabLines.First(x => x.Key == "hour");
            hourCronTabLine.InputValue.Should().Be("0");
            hourCronTabLine.Name.Should().Be("hour");
            hourCronTabLine.OutputValue.Should().Be("0");
            hourCronTabLine.Should().BeOfType<HourCronTabLine>();
        }

        [Test]
        public void When_valid_input_is_passed_it_then_it_should_return_the_correct_command()
        {
            var cronTabLines = _cronTabLineFactory.Create(VALID_COMMAND_LINE_ARGUMENTS);

            var hourCronTabLine = cronTabLines.First(x => x.Key == "command");
            hourCronTabLine.InputValue.Should().Be("/usr/bin/find");
            hourCronTabLine.Name.Should().Be("command");
            hourCronTabLine.OutputValue.Should().Be("/usr/bin/find");
            hourCronTabLine.Should().BeOfType<CommandCronTabLine>();
        }
    }
}