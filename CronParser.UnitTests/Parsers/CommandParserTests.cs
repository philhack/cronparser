﻿using System;
using CronParser.Core.Parsers;
using FluentAssertions;
using NUnit.Framework;

namespace CronParser.UnitTests.Parsers
{
    public class CommandParserTests
    {
        private IParser _commandParser;

        [SetUp]
        public void Given_a_CommandParser() {
            _commandParser = new CommandParser();    
        }

        [Test]
        public void When_a_valid_command_is_passed_in_Then_the_output_should_be_the_same_command() {
            var command = "/usr/bin/find";

            var actualOutput = _commandParser.Parse(command);

            actualOutput.Should().Be(command);
        }

        [Test]
        public void When_null_is_passed_in_Then_it_should_thow_an_exception() {
            _commandParser.Invoking(x => x.Parse(null))
                .ShouldThrow<ArgumentNullException>();

        }

        [Test]
        public void When_an_empty_string_is_passed_in_an_agument_exception_is_thrown() {
            _commandParser.Invoking(x => x.Parse(""))
                .ShouldThrow<ArgumentException>();
        }
    }
}