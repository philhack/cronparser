﻿using CronParser.Core.Parsers;
using FluentAssertions;
using NUnit.Framework;

namespace CronParser.UnitTests.Parsers
{
    public class DayOfMonthParserTests
    {
        private IParser _dayOfMonthParser;
        private string _acutalDayOfMonths;

        [SetUp]
        public void Given_a_MinuteParser()
        {
            _dayOfMonthParser = new DayOfMonthParser();
        }

        [TestCase("1", "1")]
        [TestCase("10", "10")]
        [TestCase("23", "23")]
        public void When_a_single_dayOfMonth_is_passed_in_then_it_should_return_the_single_dayOfMonth(string dayOfMonthsInput, string expectedDayOfMonths)
        {
            _acutalDayOfMonths = _dayOfMonthParser.Parse(dayOfMonthsInput);

            _acutalDayOfMonths.Should().Be(expectedDayOfMonths);
        }

        [TestCase("0")]
        [TestCase("32")]
        public void When_a_single_dayOfMonth_that_is_outside_of_the_range_if_0_to_23_is_passed_in_then_an_empty_string_is_returned(string input)
        {
            _dayOfMonthParser.Parse(input).Should().Be(string.Empty);
        }

        [Test]
        public void When_an_asertis_is_passed_in_then_all_possible_dayOfMonths_should_be_returned()
        {
            _acutalDayOfMonths = _dayOfMonthParser.Parse("*");

            _acutalDayOfMonths.Should().Be("1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31");
        }

        public void When_multiple_dayOfMonths_are_passed_in_then_the_correct_dayOfMonths_are_returned()
        {
            _acutalDayOfMonths = _dayOfMonthParser.Parse("2,6,10");

            _acutalDayOfMonths.Should().Be("2 6 10");
        }

        public void When_a_range_of_dayOfMonths_are_passed_in_then_the_correct_dayOfMonths_are_returned()
        {
            _acutalDayOfMonths = _dayOfMonthParser.Parse("10-15");

            _acutalDayOfMonths.Should().Be("10 11 12 13 14 15");
        }


        [TestCase("*/2", "2 4 6 8 10 12 14 16 18 20 22 24 26 28 30")]
        public void When_the_step_is_set_for_every_X_dayOfMonths_using_an_asterix_is_then_the_correct_dayOfMonths_are_returned(string input, string expectedOutput)
        {
            _acutalDayOfMonths = _dayOfMonthParser.Parse(input);

            _acutalDayOfMonths.Should().Be(expectedOutput);
        }

        [TestCase("1-20/3", "1 4 7 10 13 16 19")]
        public void When_the_step_is_set_for_every_X_dayOfMonths_using_an_range_within_the_step_then_the_correct_dayOfMonths_are_returned(string input, string expectedOutput)
        {
            _acutalDayOfMonths = _dayOfMonthParser.Parse(input);

            _acutalDayOfMonths.Should().Be(expectedOutput);
        }
    }
}