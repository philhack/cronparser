﻿using CronParser.Core.Parsers;
using FluentAssertions;
using NUnit.Framework;

namespace CronParser.UnitTests.Parsers
{
    public class DayOfTheWeekParserTests
    {
        private IParser _dayOfWeekParser;
        private string _acutalDayOfWeeks;

        [SetUp]
        public void Given_a_MinuteParser()
        {
            _dayOfWeekParser = new DayOfWeekParser();
        }

        [TestCase("1", "1")]
        [TestCase("7", "7")]
        public void When_a_single_dayOfWeek_is_passed_in_then_it_should_return_the_single_dayOfWeek(string dayOfWeeksInput, string expectedDayOfWeeks)
        {
            _acutalDayOfWeeks = _dayOfWeekParser.Parse(dayOfWeeksInput);

            _acutalDayOfWeeks.Should().Be(expectedDayOfWeeks);
        }

        [TestCase("-1")]
        [TestCase("24")]
        public void When_a_single_dayOfWeek_that_is_outside_of_the_range_if_0_to_23_is_passed_in_then_an_empty_string_is_returned(string input)
        {
            _dayOfWeekParser.Parse(input).Should().Be(string.Empty);
        }

        [Test]
        public void When_an_asertis_is_passed_in_then_all_possible_dayOfWeeks_should_be_returned()
        {
            _acutalDayOfWeeks = _dayOfWeekParser.Parse("*");

            _acutalDayOfWeeks.Should().Be("1 2 3 4 5 6 7");
        }

        public void When_multiple_dayOfWeeks_are_passed_in_then_the_correct_dayOfWeeks_are_returned()
        {
            _acutalDayOfWeeks = _dayOfWeekParser.Parse("2,6,10");

            _acutalDayOfWeeks.Should().Be("2 6 10");
        }

        public void When_a_range_of_dayOfWeeks_are_passed_in_then_the_correct_dayOfWeeks_are_returned()
        {
            _acutalDayOfWeeks = _dayOfWeekParser.Parse("10-15");

            _acutalDayOfWeeks.Should().Be("10 11 12 13 14 15");
        }


        [TestCase("*/2", "2 4 6")]
        public void When_the_step_is_set_for_every_X_dayOfWeeks_using_an_asterix_is_then_the_correct_dayOfWeeks_are_returned(string input, string expectedOutput)
        {
            _acutalDayOfWeeks = _dayOfWeekParser.Parse(input);

            _acutalDayOfWeeks.Should().Be(expectedOutput);
        }

        [TestCase("1-20/3", "1 4 7 10 13 16 19")]
        public void When_the_step_is_set_for_every_X_dayOfWeeks_using_an_range_within_the_step_then_the_correct_dayOfWeeks_are_returned(string input, string expectedOutput)
        {
            _acutalDayOfWeeks = _dayOfWeekParser.Parse(input);

            _acutalDayOfWeeks.Should().Be(expectedOutput);
        }
    }
}