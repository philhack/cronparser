﻿using CronParser.Core.Parsers;
using FluentAssertions;
using NUnit.Framework;

namespace CronParser.UnitTests.Parsers
{
    public class HourParserTests
    {
        private IParser _hourParser;
        private string _acutalHours;

        [SetUp]
        public void Given_a_MinuteParser()
        {
            _hourParser = new HourParser();
        }

        [TestCase("0", "0")]
        [TestCase("10", "10")]
        [TestCase("23", "23")]
        public void When_a_single_hour_is_passed_in_then_it_should_return_the_single_hour(string hoursInput, string expectedHours)
        {
            _acutalHours = _hourParser.Parse(hoursInput);

            _acutalHours.Should().Be(expectedHours);
        }

        [TestCase("-1")]
        [TestCase("24")]
        public void When_a_single_hour_that_is_outside_of_the_range_if_0_to_23_is_passed_in_then_an_empty_string_is_returned(string input)
        {
            _hourParser.Parse(input).Should().Be(string.Empty);
        }

        [Test]
        public void When_an_asertis_is_passed_in_then_all_possible_hours_should_be_returned()
        {
            _acutalHours = _hourParser.Parse("*");

            _acutalHours.Should().Be("0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23");
        }

        public void When_multiple_hours_are_passed_in_then_the_correct_hours_are_returned()
        {
            _acutalHours = _hourParser.Parse("2,6,10");

            _acutalHours.Should().Be("2 6 10");
        }

        public void When_a_range_of_hours_are_passed_in_then_the_correct_hours_are_returned()
        {
            _acutalHours = _hourParser.Parse("10-15");

            _acutalHours.Should().Be("10 11 12 13 14 15");
        }

        
        [TestCase("*/2", "0 2 4 6 8 10 12 14 16 18 20 22")]
        public void When_the_step_is_set_for_every_X_hours_using_an_asterix_is_then_the_correct_hours_are_returned(string input, string expectedOutput)
        {
            _acutalHours = _hourParser.Parse(input);

            _acutalHours.Should().Be(expectedOutput);
        }

        [TestCase("1-20/3", "1 4 7 10 13 16 19")]
        public void When_the_step_is_set_for_every_X_hours_using_an_range_within_the_step_then_the_correct_hours_are_returned(string input, string expectedOutput)
        {
            _acutalHours = _hourParser.Parse(input);

            _acutalHours.Should().Be(expectedOutput);
        }
    }
}