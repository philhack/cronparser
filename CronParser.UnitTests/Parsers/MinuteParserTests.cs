﻿using System;
using CronParser.Core.Parsers;
using FluentAssertions;
using NUnit.Framework;

namespace CronParser.UnitTests.Parsers
{
    public class MinuteParserTests
    {
        private IParser _minuteParser;
        private string _actualMinutes;

        [SetUp]
        public void Given_a_MinuteParser() {
            _minuteParser = new MinuteParser();
        }

        [TestCase("0", "0")]
        [TestCase("5", "5")]
        [TestCase("15", "15")]
        [TestCase("30", "30")]
        [TestCase("45", "45")]
        [TestCase("59", "59")]
        public void When_a_single_minute_is_passed_in_then_it_should_return_the_single_minute(string minutesInput, string expectedMinutes) {
            _actualMinutes = _minuteParser.Parse(minutesInput);

            _actualMinutes.Should().Be(expectedMinutes);
        }

        [TestCase("-1")]
        [TestCase("60")]
        public void When_a_single_minute_that_is_outside_of_the_range_if_0_to_59_is_passed_in_then_an_empty_string_is_returned(string minuteInput) {
            _minuteParser.Parse(minuteInput).Should().Be(String.Empty);
        }

        [Test]
        public void When_an_asertis_is_passed_in_then_all_possible_minutes_should_be_returned() {
            _actualMinutes = _minuteParser.Parse("*");

            _actualMinutes.Should().Be("0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 " +
                                   "20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 " +
                                   "37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 " +
                                   "54 55 56 57 58 59");
        }

        [Test]
        public void When_multiple_minutes_are_passed_in_then_the_correct_minutes_are_returned() {
            _actualMinutes = _minuteParser.Parse("15,30,45,50");

            _actualMinutes.Should().Be("15 30 45 50");
        }

        [TestCase("45,60")]
        [TestCase("2,-1")]
        public void When_two_numbers_are_passed_in_and_one_of_them_is_outside_the_range_of_0_to_59_then_an_exception_is_thrown(string inputMinutes) {
            _minuteParser.Invoking(x => x.Parse(inputMinutes))
                .ShouldThrow<ArgumentOutOfRangeException>();
        }

        [TestCase("1-5", "1 2 3 4 5")]
        [TestCase("24-31", "24 25 26 27 28 29 30 31")]
        public void When_a_range_of_minutes_are_passed_in_then_the_correct_minutes_are_returned(string inputMinutes, string expectedOutput)
        {
            _actualMinutes = _minuteParser.Parse(inputMinutes);

            _actualMinutes.Should().Be(expectedOutput);
        }

        [TestCase("*/15", "0 15 30 45")]
        [TestCase("*/2", "0 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38 40 42 44 46 48 50 52 54 56 58")]
        public void When_the_step_is_set_for_every_X_minutes_using_an_asterix_is_then_the_correct_minutes_are_returned(string stepMinuteInput, string expectedMinuteOutput)
        {
            _actualMinutes = _minuteParser.Parse(stepMinuteInput);

            _actualMinutes.Should().Be(expectedMinuteOutput);
        }

        [TestCase("1-59/2", "1 3 5 7 9 11 13 15 17 19 21 23 25 27 29 31 33 35 37 39 41 43 45 47 49 51 53 55 57 59")]
        public void When_the_step_is_set_for_every_X_minutes_using_an_range_within_the_step_then_the_correct_minutes_are_returned(string stepMinuteInput, string expectedMinuteOutput)
        {
            _actualMinutes = _minuteParser.Parse(stepMinuteInput);

            _actualMinutes.Should().Be(expectedMinuteOutput);
        }
    }
}