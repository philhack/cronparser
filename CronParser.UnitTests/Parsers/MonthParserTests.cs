﻿using CronParser.Core.Parsers;
using FluentAssertions;
using NUnit.Framework;

namespace CronParser.UnitTests.Parsers
{
    public class MonthParserTests
    {
        private IParser _monthParser;
        private string _acutalMonths;

        [SetUp]
        public void Given_a_MinuteParser()
        {
            _monthParser = new MonthParser();
        }

        [TestCase("1", "1")]
        [TestCase("12", "12")]
        public void When_a_single_month_is_passed_in_then_it_should_return_the_single_month(string monthsInput, string expectedMonths)
        {
            _acutalMonths = _monthParser.Parse(monthsInput);

            _acutalMonths.Should().Be(expectedMonths);
        }

        [TestCase("0")]
        [TestCase("13")]
        public void When_a_single_month_that_is_outside_of_the_range_if_0_to_23_is_passed_in_then_an_empty_string_is_returned(string input)
        {
            _monthParser.Parse(input).Should().Be(string.Empty);
        }

        [Test]
        public void When_an_asertis_is_passed_in_then_all_possible_months_should_be_returned()
        {
            _acutalMonths = _monthParser.Parse("*");

            _acutalMonths.Should().Be("1 2 3 4 5 6 7 8 9 10 11 12");
        }

        public void When_multiple_months_are_passed_in_then_the_correct_months_are_returned()
        {
            _acutalMonths = _monthParser.Parse("2,6,10");

            _acutalMonths.Should().Be("2 6 10");
        }

        public void When_a_range_of_months_are_passed_in_then_the_correct_months_are_returned()
        {
            _acutalMonths = _monthParser.Parse("10-15");

            _acutalMonths.Should().Be("10 11 12 13 14 15");
        }


        [TestCase("*/2", "2 4 6 8 10 12")]
        public void When_the_step_is_set_for_every_X_months_using_an_asterix_is_then_the_correct_months_are_returned(string input, string expectedOutput)
        {
            _acutalMonths = _monthParser.Parse(input);

            _acutalMonths.Should().Be(expectedOutput);
        }

        [TestCase("1-20/3", "1 4 7 10 13 16 19")]
        public void When_the_step_is_set_for_every_X_months_using_an_range_within_the_step_then_the_correct_months_are_returned(string input, string expectedOutput)
        {
            _acutalMonths = _monthParser.Parse(input);

            _acutalMonths.Should().Be(expectedOutput);
        }
    }
}