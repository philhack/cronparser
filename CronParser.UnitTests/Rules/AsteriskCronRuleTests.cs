﻿using CronParser.Core.Rules;
using CronParser.UnitTests.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace CronParser.UnitTests.Rules
{
    public class AsteriskCronRuleTests
    {
        private ICronRule _rule;

        [SetUp]
        public void Given_an_asterisk_cron_rule() {            
            _rule = new AsteriskCronRule(AllPossibleItemsTestHelper.NumbersZeroToSixteen);
        }

        [TestCase("*", "0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16", true)]
        [TestCase("", "", false)]
        [TestCase("**", "", false)]
        public void When_input_is_passed_in_then_the_expected_result_is_returned(string input, string expected, bool expectedSuccess) {
            _rule.Access(new CronRuleAssessment(input)).Success.Should().Be(expectedSuccess);
            _rule.Access(new CronRuleAssessment(input)).Value.Should().Be(expected);
        }
    }
}