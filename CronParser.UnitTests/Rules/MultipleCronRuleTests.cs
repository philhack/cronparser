﻿using CronParser.Core.Rules;
using CronParser.UnitTests.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace CronParser.UnitTests.Rules
{
    public class MultipleCronRuleTests
    {
        private ICronRule _rule;

        [SetUp]
        public void Given_an_asterisk_cron_rule()
        {
            _rule = new MultipleCronRule(AllPossibleItemsTestHelper.NumbersZeroToSixteen);
        }

        [TestCase("10,14", "10 14", true)]
        public void When_input_is_passed_in_then_the_expected_result_is_returned(string input, string expected, bool expectedSuccess)
        {
            _rule.Access(new CronRuleAssessment(input)).Success.Should().Be(expectedSuccess);
            _rule.Access(new CronRuleAssessment(input)).Value.Should().Be(expected);
        }

    }
}