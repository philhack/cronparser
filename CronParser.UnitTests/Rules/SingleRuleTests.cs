﻿using CronParser.Core.Rules;
using CronParser.UnitTests.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace CronParser.UnitTests.Rules
{
    public class SingleRuleTests
    {
        private ICronRule _rule;

        [SetUp]
        public void Given_an_asterisk_cron_rule()
        {
            _rule = new SingleCronRule(AllPossibleItemsTestHelper.NumbersZeroToSixteen);
        }

        [TestCase("6", "6", true)]
        [TestCase("3", "3", true)]
        public void When_input_is_passed_in_then_the_expected_result_is_returned(string input, string expected, bool expectedSuccess)
        {
            _rule.Access(new CronRuleAssessment(input)).Success.Should().Be(expectedSuccess);
            _rule.Access(new CronRuleAssessment(input)).Value.Should().Be(expected);
        }
    }
}