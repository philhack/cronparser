﻿using System;
using CronParser.Core.Validators;
using FluentAssertions;
using NUnit.Framework;

namespace CronParser.UnitTests.Validators
{
    public class CronInputValidatorTests
    {
        private CronInputValidator _cronInputValidator;

        [SetUp]
        public void Given_a_CronInput_parser() {
            _cronInputValidator = new CronInputValidator();
        }

        [Test]
        public void When_too_many_argments_are_passed_in_then_an_exception_should_be_thrown() {
            var tooManyArgments = "5 */15 0 1,15 * 1-5 /usr/bin/find";

            _cronInputValidator.Invoking(x => x.Validate(tooManyArgments))
                .ShouldThrow<ArgumentException>();
        }

        [Test]
        public void When_too_few_argments_are_passed_in_then_an_exception_should_be_thrown() {
            var tooFewArguments = "5";

            _cronInputValidator.Invoking(x => x.Validate(tooFewArguments))
                .ShouldThrow<ArgumentException>();
        }

        [Test]
        public void When_null_is_passed_in_then_an_exception_should_be_thrown() {
            _cronInputValidator.Invoking(x => x.Validate(null))
                .ShouldThrow<ArgumentNullException>();
        }

        [Test]
        public void When_a_valid_input_is_passed_in_then_no_errors_should_be_correct()
        {
            var commandLineArgument = "*/15 0 1,15 * 1-5 /usr/bin/find";

            _cronInputValidator.Invoking(x => x.Validate(commandLineArgument))
                .ShouldNotThrow<Exception>();
        }

        [Test]
        public void When_arguments_are_passed_in_without_being_separated_by_a_space_are_passed_in_then_an_exception_should_be_thrown()
        {
            var commandLineArgument = "*/15_0_1,15_*_1-5_/usr/bin/find";

            _cronInputValidator.Invoking(x => x.Validate(commandLineArgument))
                .ShouldThrow<ArgumentException>();
        }
    }
}