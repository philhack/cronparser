# Cron Parser

## Usage
This is a [framework dependent deployment](https://docs.microsoft.com/en-us/dotnet/core/deploying/index), therefore .net core 2.0 needs to be installed on the target machine.

Example usage:
```
dotnet CronParser.Console.dll "minute hour dayOfMonth month dayOfWeek command"
dotnet CronParser.Console.dll "*/15 0 1,15 * 1-5 /usr/bin/find"
```
